package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import Connection.ConnectionFactory;
import Model.Order;

public class OrderDao {

    protected static final Logger LOGGER = Logger.getLogger(ClientDao.class.getName());
    private static final String insertStatementString = "INSERT INTO order (number,table,value)"
            + " VALUES (?,?,?,?)";
    private final static String findStatementString = "SELECT * FROM order where number = ?";
    private final static String deleteStatementString = "DELETE FROM order WHERE number = ?";
    private final static String updateStatementString = "UPDATE order SET table = ?, value = ? WHERE number = ?";

    public static Order findByNumber(int orderNumber) {
        Order toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, orderNumber);
            rs = findStatement.executeQuery();
            rs.next();

            int table = rs.getInt("table");
            int value = rs.getInt("value");
            toReturn = new Order(orderNumber, table, value);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static int insert(Order order) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, String.valueOf(order.getNumber()));
            insertStatement.setString(2, String.valueOf(order.getTable()));
            insertStatement.setString(3, String.valueOf(order.getValue()));
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static void delete(Order order) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);

            deleteStatement.setString(1, String.valueOf(order.getNumber()));
            deleteStatement.executeUpdate();


        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
    }

    public static void main(String[] args) {
        try {
            Connection db = ConnectionFactory.getConnection();
        }
        catch(Exception e)
            {
            e.printStackTrace();
        }
    }
}

