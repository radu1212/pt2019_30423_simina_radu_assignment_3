package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import Connection.ConnectionFactory;
import Model.Client;

public class ClientDao {


        protected static final Logger LOGGER = Logger.getLogger(ClientDao.class.getName());
        private static final String insertStatementString = "INSERT INTO client (name,address,email,age)"
                + " VALUES (?,?,?,?)";
        private final static String findStatementString = "SELECT * FROM client where id = ?";

        public static Client findById(int clientId) {
            Client toReturn = null;

            Connection dbConnection = ConnectionFactory.getConnection();
            PreparedStatement findStatement = null;
            ResultSet rs = null;
            try {
                findStatement = dbConnection.prepareStatement(findStatementString);
                findStatement.setLong(1, clientId);
                rs = findStatement.executeQuery();
                rs.next();

                String name = rs.getString("name");
                int orderId = rs.getInt("orderid");
                toReturn = new Client(clientId, name, orderId);
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
            } finally {
                ConnectionFactory.close(rs);
                ConnectionFactory.close(findStatement);
                ConnectionFactory.close(dbConnection);
            }
            return toReturn;
        }

        public static int insert(Client client) {
            Connection dbConnection = ConnectionFactory.getConnection();

            PreparedStatement insertStatement = null;
            int insertedId = -1;
            try {
                insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
                insertStatement.setString(1, String.valueOf(client.getId()));
                insertStatement.setString(2, client.getName());
                insertStatement.setString(3, String.valueOf(client.getOrderId()));
                insertStatement.executeUpdate();

                ResultSet rs = insertStatement.getGeneratedKeys();
                if (rs.next()) {
                    insertedId = rs.getInt(1);
                }
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
            } finally {
                ConnectionFactory.close(insertStatement);
                ConnectionFactory.close(dbConnection);
            }
            return insertedId;
        }


}
