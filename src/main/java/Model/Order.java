package Model;

public class Order {

    private int number;
    private int table;
    private int value;

    public Order(int number, int table, int value) {
        this.number = number;
        this.table = table;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Order{" +
                "number=" + number +
                ", table=" + table +
                ", value=" + value +
                '}';
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
